N9 Simulator
============

Open the `N9 Simulator` shortcut to start the simulator. It will automatically connect to any `ada_core` scripts that
 are run after it is opened.